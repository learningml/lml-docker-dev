# LML development environment docker based

This is a docker-compose file with the information needed to get up a
fully functional development environment with all the projects composing
LearningML:

- learningml-editor
- lml-scratch-gui
- lml-scratch-vm
- lml-scratch-l10n
- lml-snap

This development environment also includes a nginx docker (lml-docker) working
as a reverse proxy of all the web applications. This is important to allow all the apps be served under the same domain, which allows to share the Localstorage mechanism among them. This is mandatory for the correct operation of the system, 
as explained in https://web.learningml.org/en/connect-learningml-editor-with-lml-scratch/.

## How to use the development environment 

Create a directory where all the repos will be cloned. And change to such directory.

```
# mkdir lml
# cd lml
```

Clone this project:

```
# git clone https://gitlab.com/learningml/lml-docker-dev
```

Enter into `lml-docker-dev`directory:

```
# cd lml-docker-dev
```

Clone all the LML repos and prepare the projects:

```
# make prepare
```

This command will clone the repos and will change the branch of `learningml-editor` and `lml-snap` to `develop`, since the correct Dockerfiles are in this branche.


Build the images and creates the containers

```
# make build
```

You can enter into learningml-editor through:

```
http://localhost:8888
```

Take into account that `learningml-editor` container takes some time before compile and run. So you must wait a little time to have everything working.

Finally yo can stop container:

```
# make stop
```

Delete them:

```
# make rm
````

Or get up the containers (without image rebuilding):

```
# make up
```