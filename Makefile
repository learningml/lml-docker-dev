prepare:
	rm -rf ../learningml-editor ../lml-snap ../lml-scratch*
	git clone git@gitlab.com:learningml/learningml-editor ../learningml-editor
	git clone git@gitlab.com:learningml/lml-snap ../lml-snap
	git clone git@gitlab.com:learningml/lml-scratch-gui ../lml-scratch-gui
	git clone git@gitlab.com:learningml/lml-scratch-vm ../lml-scratch-vm
	git clone git@gitlab.com:learningml/lml-scratch-l10n ../lml-scratch-l10n
	cd ../learningml-editor && git checkout develop
	cp DockerfileScratch ..

build:
	docker-compose up -d --build

up:
	docker-compose up -d

status:
	docker-compose status

stop:
	docker-compose stop

rm:
	docker-compose stop
	docker-compose rm

restart:
	docker-compose restart
